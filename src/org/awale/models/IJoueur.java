package org.awale.models;

public interface IJoueur {
	
	void commencer( int numeroTroue);
	void ajouterRecolte(int recolte);
	int getRecolte();

}
