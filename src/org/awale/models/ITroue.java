package org.awale.models;

public interface ITroue {
	boolean estInitialise();

	void reset();

	int recolter();
	
	boolean aDeuxTroisGraines();

	void increment();	

	boolean estVide();
}
