package org.awale.models;

public class Partie {
	private IJoueur [] joueurs; 
	private ITroue [] troues;
	private int EstSensHoraire=1;
	private int coteQuiJoue = 0; 

	public Partie(IJoueur[] joueurs, ITroue[] troues) {
		super();
		this.joueurs = joueurs;
		this.troues = troues;
	}

	public IJoueur[] getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(IJoueur[] joueurs) {
		this.joueurs = joueurs;
	}

	public ITroue[] getTroues() {
		return troues;
	}

	public void setTroues(ITroue[] troues) {
		this.troues = troues;
	}

	public Partie() {
		this.joueurs = new IJoueur[2];
		this.troues = new ITroue[12];
	}

	public int getEstSensHoraire() {
		
		return this.EstSensHoraire;
	}

	public int getCoteQuiJoue() {
		
		return coteQuiJoue;
	}

}
