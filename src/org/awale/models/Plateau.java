package org.awale.models;

public class Plateau implements IPlateau{
	private ITroue [] troues;
	private IJoueur[] joueurs;
	private static int NOMBRE_TROUE = 6;
	private static int NOMBRE_JOUEUR_MAX = 2;
	private int estSensHoraire = 1;
	private int coteQuiJoue= 0;
	
	public Plateau() {
		
		creerPlateau();
	}

	private void creerPlateau() {
		joueurs = new Joueur[NOMBRE_JOUEUR_MAX];
		troues = new Troue [NOMBRE_JOUEUR_MAX * NOMBRE_TROUE];
		for(int i  = 0; i < NOMBRE_JOUEUR_MAX * NOMBRE_TROUE; i ++ )
			troues[i] =new Troue();	
		
	}

	@Override
	public void reset() {
	
			for(ITroue troue : troues){
				troue.reset();
			}
			
	}

	@Override
	public boolean estRenitialise() {
		boolean estReinitialise = true;
		for(ITroue troue : troues){
				estReinitialise = estReinitialise && troue.estInitialise();
			
		}
		return estReinitialise;
	}

	@Override
	public void demarrer() {
		this.joueurs[0] = new Joueur();
		this.joueurs[1] = new Joueur();
		this.reset();
	}

	public ITroue[] getTroues() {
		return troues;
	}

	private void setTroues(ITroue[] troues) {
		this.troues = troues;
	}

	public IJoueur[] getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(IJoueur[] joueurs) {
		this.joueurs = joueurs;
	}

	@Override
	public void initialise(Partie partie) {
		for (int i = 0; i < partie.getJoueurs().length; i++) {
			if (partie.getJoueurs()[i] == null) {
				partie.getJoueurs()[i] = new  Joueur();
			} 
		}
		for (int i = 0; i < partie.getTroues().length; i++) {
			if (partie.getTroues()[i] == null) {
				partie.getTroues()[i] = new Troue();
			} 
		}
		
		this.setJoueurs(partie.getJoueurs());
		this.setTroues(partie.getTroues());
		this.estSensHoraire = partie.getEstSensHoraire();
		this.coteQuiJoue = partie.getCoteQuiJoue();
		
		
		
	}

	@Override
	public Partie getPartie() {
		return new Partie(this.getJoueurs(), this.getTroues());
	}

	@Override
	public void jouer(int joueur, int caseDepart) throws Exception {
		int cage = caseDepart;
		if(joueur > 1 || cage>=12)throw new Exception();
		if (joueur != coteQuiJoue)
			throw new Exception();
		ITroue depart = this.troues[cage];
		
		do {	
			int nombrePierres = depart.recolter();
			while (nombrePierres > 0) {
				cage = (cage + 1 * this.estSensHoraire + 12) % (NOMBRE_JOUEUR_MAX * NOMBRE_TROUE);
				if (cage != caseDepart) {
					this.troues[cage].increment();
					nombrePierres--;
				}
			};
			if (this.troues[cage].aDeuxTroisGraines() && !this.leJouerEstDeSonCote(cage) ) {
				int nombreDeCailloux = this.troues[cage].recolter();
				joueurs[coteQuiJoue].ajouterRecolte(nombreDeCailloux);
				cage = (cage - 1 * this.estSensHoraire + 12)% (NOMBRE_JOUEUR_MAX * NOMBRE_TROUE);
			} 
			depart = this.troues[cage];
			if(!depart.aDeuxTroisGraines()|| this.leJouerEstDeSonCote(cage))
				coteQuiJoue = (coteQuiJoue+1)%2;
			
			
		} while ( coteQuiJoue == joueur );
		
	}
	
	private boolean leJouerEstDeSonCote(int numeroCage){
		
		return (int)(numeroCage/6) == coteQuiJoue;
	}

	@Override
	public void choisierSens(boolean estSensHoraire) {
		this.estSensHoraire = estSensHoraire? 1 : -1;
	}

	@Override
	public void choisirQuiDebut(boolean cotePaireDebute) {
		this.coteQuiJoue = cotePaireDebute? 0:1;
		
	}

	@Override
	public IJoueur getGagnant() {				
		return this.joueurs[obtenirGagnants()[0]];
	}

	@Override
	public boolean partieTerminee() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public IJoueur getPerdant() {
		return this.joueurs[obtenirGagnants()[1]];
	}
	
	private int[] obtenirGagnants(){
		int [] gagnants = new int [2];
		if(this.joueurs[0].getRecolte() > this.joueurs[1].getRecolte() 
				&& this.partieTerminee()){			
			gagnants[0]= 0;
			gagnants[1]= 1;		
		}
		else if(this.joueurs[0].getRecolte() < this.joueurs[1].getRecolte() 
				&& this.partieTerminee()) {
			gagnants[0]= 1;
			gagnants[1]= 0;		
		}	
		return gagnants;
	}

	
	
}
