package org.awale.models;

public interface IPlateau {
	
	void reset();
	
	void initialise(Partie partie);
	
	Partie getPartie();
	
	void jouer(int joueur, int cage) throws Exception;

	boolean estRenitialise();
	
	void choisierSens(boolean estSensHoraire); 
	
	void choisirQuiDebut(boolean cotePaireDebute);
	
	void demarrer();
	IJoueur getGagnant();

	boolean partieTerminee();

	IJoueur getPerdant();

	void setJoueurs(IJoueur[] joueurs);

}
