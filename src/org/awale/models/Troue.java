package org.awale.models;

public class Troue implements ITroue {
	private static int NOMBRE_CAILLOUX_INITIAL = 4;
	private int nombreCailloux;
	
	public int getNombreCailloux() {
		return nombreCailloux;
	}

	public void setNombreCailloux(int nombreCailloux) {
		this.nombreCailloux = nombreCailloux;
	}

	public Troue() {
		nombreCailloux = 0;
	}

	public boolean estInitialise() {		
		return this.getNombreCailloux() == NOMBRE_CAILLOUX_INITIAL;
	}

	@Override
	public void reset() {
		this.setNombreCailloux(NOMBRE_CAILLOUX_INITIAL);
		
	}

	@Override
	public int recolter() {
		int nombrePierres = this.getNombreCailloux();
		this.setNombreCailloux(0);
		return nombrePierres;
	}

	@Override
	public boolean aDeuxTroisGraines() {	
		return this.nombreCailloux == 2 || this.nombreCailloux == 3 ;
	}

	@Override
	public void increment() {
		this.nombreCailloux++;
		
	}



	@Override
	public boolean estVide() {
		// TODO Auto-generated method stub
		return this.nombreCailloux ==0;
	}

}
