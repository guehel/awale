package org.awale.test;

import static org.junit.Assert.*;

import org.awale.models.ITroue;
import org.awale.models.Troue;
import org.junit.Before;
import org.junit.Test;

public class TroueTest {
	
	private ITroue troue;

	@Before
	public void setUp() throws Exception {
		troue = new Troue();
	}

	@Test
	public void reset_remet() {
		troue.reset();
		assert(troue.estInitialise());
	}

}
