package org.awale.test;

import static org.junit.Assert.*;

import org.awale.models.*;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class PlateauTest {
	private IPlateau plateau; 	
	@Before
	public void setUp() throws Exception {
		plateau = new Plateau();
	}

	@Test
	public void resetReinitialiseLesTroues() {
		plateau.reset();		
		assert(plateau.estRenitialise());
		
	}
	
	@Test
	public void demarrerCreerPartie() {
		plateau.demarrer();		
		assert(plateau.estRenitialise());
		
	}
	
	@Test
	public void jouerversProchainee() {
		int joueur = 0; 
		int  cage =   0;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(true);
		
		try {
			plateau.jouer(joueur, cage);
			assertTrue(!plateau.getPartie().getTroues()[1].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[2].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[3].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[4].estInitialise());
			
			assertTrue((plateau.getPartie().getTroues()[1].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[2].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[3].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[4].recolter()==5));
			
			
		} catch (Exception e) {
						e.printStackTrace();
		}
	}
	
	@Test
	public void jouerantiversProchainee() {
		int joueur = 0; 
		int  cage =   0;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(false);
		
		try {
			plateau.jouer(joueur, cage);
			assertTrue(!plateau.getPartie().getTroues()[11].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[10].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[9].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[8].estInitialise());
			
			assertTrue((plateau.getPartie().getTroues()[11].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[10].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[9].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[8].recolter()==5));
			
			
		} catch (Exception e) {
						e.printStackTrace();
		}
	}
	
	
	@Test
	public void jouerChangeJoueur() {
		int joueur = 0; 
		int  cage =   5;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(true);
		
		try {
			plateau.jouer(joueur, cage);
			assertTrue(!plateau.getPartie().getTroues()[6].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[7].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[8].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[9].estInitialise());
			
			assertTrue((plateau.getPartie().getTroues()[6].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[7].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[8].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[9].recolter()==5));
			
			
		} catch (Exception e) {
						e.printStackTrace();
		}
	}
	@Test
	public void jouerAntiChangeJoueur() {
		int joueur = 0; 
		int  cage =   5;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(false);
		
		try {
			plateau.jouer(joueur, cage);
			assertTrue(!plateau.getPartie().getTroues()[4].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[3].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[2].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[1].estInitialise());
			
			assertTrue((plateau.getPartie().getTroues()[4].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[3].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[2].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[1].recolter()==5));
			
			
		} catch (Exception e) {
						e.printStackTrace();
		}
	}
	
	
	@Test
	public void jouerA11ChangeCote() {
		int joueur = 0; 
		int  cage =   11;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(true);
		
		try {
			plateau.jouer(joueur, cage);
			assertTrue(!plateau.getPartie().getTroues()[0].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[1].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[2].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[3].estInitialise());
			
			assertTrue((plateau.getPartie().getTroues()[0].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[1].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[2].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[3].recolter()==5));
			
			
		} catch (Exception e) {
						e.printStackTrace();
		}
	}

	@Test
	public void jouerAntiA11ChangeCote() {
		int joueur = 0; 
		int  cage =   11;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(false);
		
		try {
			plateau.jouer(joueur, cage);
			assertTrue(!plateau.getPartie().getTroues()[10].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[9].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[8].estInitialise());
			assertTrue(!plateau.getPartie().getTroues()[9].estInitialise());
			
			assertTrue((plateau.getPartie().getTroues()[10].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[9].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[8].recolter()==5));
			assertTrue((plateau.getPartie().getTroues()[7].recolter()==5));
			
			
		} catch (Exception e) {
						e.printStackTrace();
		}
	}
	
	@Test
	public void jouerCreerPartie() {
		int joueur = 0; 
		int cage =   2;
		plateau.choisirQuiDebut(true);
		try {
			plateau.jouer(joueur, cage);
		} catch (Exception e) {
			fail();
		}		
		assert(!plateau.estRenitialise());
		
	}
	
	@Test
	public void jouerSur3contirecolte() {
		int joueur = 0; 
		int  cage =   3;
		plateau.demarrer();
		plateau.choisirQuiDebut(true);
		plateau.choisierSens(true);
		
		try {
			plateau.jouer(joueur, 0);
			plateau.jouer(joueur+1, 11);
			plateau.jouer(joueur, 8);
		
			assertTrue(!plateau.getPartie().getTroues()[11].estVide());
			assertTrue(!(plateau.getPartie().getTroues()[0].recolter()==1));
			
		} catch (Exception e) {
						fail();
		}
	}
	
	@Test
	public void getGagnant_PremierJoueurgagnant_retourneGagant(){
		//Arrange
		IJoueur resultat;
		IJoueur gagnant = new Joueur(); 
		gagnant.ajouterRecolte(1);
		IJoueur perdant = new Joueur(); 
		perdant.ajouterRecolte(0);
		IJoueur [] joueurs = {gagnant,perdant };
		plateau.setJoueurs(joueurs);
		
		//agir
		resultat = plateau.getGagnant();
		
		//Assert
		assertTrue(resultat.equals(gagnant));
		assertTrue(resultat != null);
		assertTrue(plateau.partieTerminee());
		assertTrue(resultat.getRecolte() > plateau.getPerdant().getRecolte() );
		
	}
	
	@Test
	public void getGagnant_PremierJoueurgagnant_retournenull(){
		//Arrange
		IJoueur resultat;
		IJoueur gagnant = new Joueur(); 
		gagnant.ajouterRecolte(1);
		IJoueur perdant = new Joueur(); 
		perdant.ajouterRecolte(1);
		IJoueur [] joueurs = {gagnant,perdant };
		plateau.setJoueurs(joueurs);
		
		//agir
		resultat = plateau.getGagnant();
		
		//Assert
	
		assertTrue(resultat == null);
		
		
	}
	
	@Test
	public void onSauteSaCaseDeDepart(){
		ITroue [] troues = new Troue[12];
		troues[0]= new Troue();
		for (int i = 0; i <12; i++) {
			troues[0].increment();
		}
		Partie partie = new Partie();
		partie.setTroues(troues);
		plateau.initialise(partie);
		try {
			plateau.jouer(0, 0);
			assertTrue((plateau.getPartie().getTroues()[0].recolter()==0));
			assertTrue((plateau.getPartie().getTroues()[1].recolter()==2));
		} catch (Exception e) {
			fail();
		}
		
	}
	
	
}
